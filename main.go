package main

import (
	"flag"
	"net/http"
	"log"
	"fmt"
)

func main() {
	port := flag.Int("port", 80, "port to serve file")
	dir := flag.String("directory", "static/", "directory of files")
	flag.Parse()

	fs := http.Dir(*dir)
	handler := http.FileServer(fs)
	http.Handle("/", handler)

	log.Printf("Running on port %d\n", *port)

	addr := fmt.Sprintf("127.0.0.1:%d", *port)

	err := http.ListenAndServe(addr,nil)
	fmt.Println(err.Error())
}
